FROM quay.io/wildfly/wildfly

ADD target/helloworld-7.1.0.GA.war /opt/jboss/wildfly/standalone/deployments/helloworld.war

RUN /opt/jboss/wildfly/bin/add-user.sh admin welcome1 --silent

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]